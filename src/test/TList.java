package test;

import junit.framework.TestCase;
import model.data_structures.ListaEnlazada;

public class TList extends TestCase{
	
	private ListaEnlazada<String> linkedList;
	
	private void setupEscenario1()
	{
		linkedList = new ListaEnlazada<String>();
		linkedList.add("String 1");
		linkedList.add("String 2");
		linkedList.add("String 3");
		linkedList.add("String 4");
	}
	
	public void testAdd()
	{
		setupEscenario1();
		linkedList.add("String 5");
		assertEquals(5, linkedList.getSize());
	}
	
	public void testErase()
	{
		setupEscenario1();
		linkedList.erase("String 4");;
		assertEquals(3, linkedList.getSize());
	}
	

	public void testGetElemento()
	{
		setupEscenario1();
		linkedList.getElement(2);
		assertEquals("String 2", linkedList.getElement(2));
	}
	
	public void testGetSize()
	{
		setupEscenario1();
		linkedList.getSize();
		assertEquals(4, linkedList.getSize());
	}
	
	public void testIsEmpty()
	{
		setupEscenario1();
		linkedList.isEmpty();
		assertEquals(false, linkedList.isEmpty());
	}

}
