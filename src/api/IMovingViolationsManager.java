package api;

import model.data_structures.*;
import model.vo.VOMovingViolations;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IMovingViolationsManager {

	/**
	 * Method to load the Moving Violations of the STS
	 * @param movingViolationsFile - path to the file 
	 * @throws Exception 
	 */
	void loadMovingViolations(String movingViolationsFile) throws Exception;
	
	public ILinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode);
	
	
	public ILinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator);

	
}
