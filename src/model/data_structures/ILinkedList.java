package model.data_structures;

/**
 * Generic LinkedList
 * @param <T>
 */
public interface ILinkedList<T extends Comparable <T>> extends Iterable<T>{
	
	/**
	 * Anhande el elmento a una posicion de la lista
	 * @param item Item a anhadir
	 */
	public void add(T item);
	
	/**
	 * Elimina un elemento de la lista dada por parametro
	 * @param item elemento a eliminar
	 */
	public void erase(T item);
	
	/**
	 * Retorna el elemento dado por parametro
	 * @param Integer elemento a obtener
	 * @return el elemento eliminado
	 */
	public T getElement(Integer numero);
	/**
	 * Retorna el tamanho de lista 
	 * @return Tamanho de la lista
	 */
	public int getSize();
	/**
	 * Retorna si la lista esta vacia o no
	 * @return True si la lista esta Vac�a, False de lo contrario
	 */
	public boolean isEmpty();

}
