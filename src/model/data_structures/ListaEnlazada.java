package model.data_structures;

import java.util.Iterator;

import org.omg.IOP.TAG_RMI_CUSTOM_MAX_STREAM_FORMAT;

import com.sun.xml.internal.bind.v2.runtime.reflect.ListIterator;

import sun.awt.util.IdentityLinkedList;

public class ListaEnlazada<T extends Comparable<T>> implements ILinkedList<T> {

	private Nodo<T> first = null;
	
	private int size = 0;
	
	public void addFirst(T item)
	{
		Nodo<T> nodo = new Nodo(item);
		nodo.siguiente = first;
		first = nodo;
	}
	
	public void addLast(T item)
	{
		Nodo<T> nodo = new Nodo(item);
		Nodo<T> puntero = first;
		
		while(puntero.siguiente != null)
		{
			puntero = puntero.siguiente;
		}
		puntero.siguiente = nodo;
	}
	
	@Override
	public void add(T item) {
		// TODO Auto-generated method stub
		if(isEmpty())
		{
			addFirst(item);
			size++;
		}
		else
		{
			addLast(item);
			size++;
		}
	}

	@Override
	public void erase(T item) {
		// TODO Auto-generated method stub
		Nodo<T> puntero = first;
		Nodo<T> anterior = null;
		
		if(!isEmpty())
		{
			while(puntero.item.compareTo(item)!= 0)
			{
				anterior = puntero;
				puntero = puntero.siguiente;
			}
			anterior.siguiente = null;
		}
	}

	@Override
	public T getElement(Integer numero) {
		// TODO Auto-generated method stub
		Nodo<T> nodo = null;
		Nodo<T> puntero = first;
		Integer contador = 1;
		
		while(contador != numero)
		{
			puntero = puntero.siguiente;
			contador++;
		}
		
		nodo = puntero;
		
		return (T) nodo;
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(size == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private class IteradorLista<T extends Comparable<T>> implements Iterator<T> {

		private Nodo<T> proximo;
		
		/**
		* Iniciar el Iterador con el primer nodo por visitar
		 * @return 
		*/
		public IteradorLista( Nodo<T> primero )
		{
		proximo = primero; // primero/pr�ximo a visitar
		}
		

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public T next() {
			// TODO Auto-generated method stub
			return null;
		}

	} 
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteradorLista<T>(first);
	}

}
