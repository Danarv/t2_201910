package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.opencsv.bean.CsvToBeanBuilder;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import model.data_structures.*;

public class MovingViolationsManager implements IMovingViolationsManager {
	
	public static final String DATOS = "./data/Moving_Violations_Issued_in_January_2018.csv";
	
	ListaEnlazada<VOMovingViolations> movingViolations;

	
	public void loadMovingViolations(String movingViolationsFile) throws Exception
	{
		// TODO Auto-generated method stub
		movingViolations = new ListaEnlazada<VOMovingViolations>();
		
		File flMovingViolations = new File(movingViolationsFile);
		FileReader fr = new FileReader(flMovingViolations);
		BufferedReader lector = new BufferedReader(fr);
		lector.readLine();
		
		String info = lector.readLine();
		
		while(info != null)
		{
			ListaEnlazada<String> movingViolationsInfo = new ListaEnlazada<String>();
			String[] mv = info.split(",");
			
			String objectId = mv[0].trim();
			String row = mv[1].trim();
			String location = mv[2].trim();
			String addressId = mv[3].trim();
			String streetsegId = mv[4].trim();
			String xCoord = mv[5].trim();
			String yCoord = mv[6].trim();
			String ticketType= mv[7].trim();
			String fineAMT = mv[8].trim();
			String totalPaid = mv[9].trim();
			String penalty1 = mv[10].trim();
			String penalty2 = mv[11].trim();
			String accidentIndicator = mv[12].trim();
			String ticketIssueDate = mv[13].trim();
			String violationCode = mv[14].trim();
			String violationDescription = mv[15].trim();
			String rowId = mv[16].trim();
			
			movingViolationsInfo.add(objectId);
			movingViolationsInfo.add(row);
			movingViolationsInfo.add(location);
			movingViolationsInfo.add(addressId);
			movingViolationsInfo.add(streetsegId);
			movingViolationsInfo.add(xCoord);
			movingViolationsInfo.add(yCoord);
			movingViolationsInfo.add(ticketType);
			movingViolationsInfo.add(fineAMT);
			movingViolationsInfo.add(totalPaid);
			movingViolationsInfo.add(penalty1);
			movingViolationsInfo.add(penalty2);
			movingViolationsInfo.add(accidentIndicator);
			movingViolationsInfo.add(ticketIssueDate);
			movingViolationsInfo.add(violationCode);
			movingViolationsInfo.add(violationDescription);
			movingViolationsInfo.add(rowId);
			
			movingViolations.add(new VOMovingViolations(Integer.parseInt(movingViolationsInfo.getElement(1)), movingViolationsInfo.getElement(3), movingViolationsInfo.getElement(14), Integer.parseInt(movingViolationsInfo.getElement(10)), movingViolationsInfo.getElement(13), movingViolationsInfo.getElement(15), movingViolationsInfo.getElement(16)));
		}
		lector.close();
		fr.close();
	}

		
	@Override
	public ILinkedList <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		// TODO Auto-generated method stub
		VOMovingViolations movingViolation = null;
		ListaEnlazada<VOMovingViolations> violationsByCode = new ListaEnlazada<VOMovingViolations>();
		
		while(movingViolations.iterator().hasNext())
		{
			movingViolation = movingViolations.iterator().next();
			
			if(movingViolation.getViolationCode().compareTo(violationCode) == 0)
			{
				violationsByCode.add(movingViolation);
			}
		}
		return violationsByCode;
	}

	@Override
	public ILinkedList <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		// TODO Auto-generated method stub
		VOMovingViolations movingViolation = null;
		ListaEnlazada<VOMovingViolations> violationsByAccident = new ListaEnlazada<VOMovingViolations>();
		
		while(movingViolations.iterator().hasNext())
		{
			movingViolation = movingViolations.iterator().next();
			
			if(movingViolation.getViolationCode().compareTo(accidentIndicator) == 0)
			{
				violationsByAccident.add(movingViolation);
			}
		}
		return violationsByAccident;
	}	


}
